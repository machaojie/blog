---
layout:     post
title:      "ButterKnife生成文件ID错误"
date:       2017-08-15 15:00:00
author:     "machaojie"
header-img: "img/0815.jpg"
---

  昨晚测试Android项目时遇到一个闪退问题，画面点进去直接挂掉，崩溃信息类似下面
>java.lang.IllegalStateException: Required view 'et_inspection_section_header_total' with ID 2131624440 for field 'address1' was not found. If this view is optional add '@Nullable' (fields) or '@Optional' (methods) annotation.

 在activity中声明的控件如下所示：
  > @BindView(R2.id.main_layout)
PinnedSectionListView mListView

  因为项目里用到了ButterKnife框架，因此检查了下崩溃画面的view_Binding文件，在view_Binding文件中生成的代码如下所示：
>target.mListView = Utils.findRequiredViewAsType(source, R.id.shop_tip, "field 'mListView'", PinnedSectionListView.class);

  可以看到生成的view_Binding文件中的id并不是我声明的main_layout，检查了下main_layout和shop_tip的声明，发现两个变量在R文件中的id是同一个。是不是因为这样ButterKnife生成的view_Binding文件才会出现问题。在找了一会还没找到解决方法后，突然想到肯定也有其他人遇到这个问题，github上会不会有人提过。跑到大神JakeWharton的github上看了下ButterKnife的Issue，果然有人提了问题。
  
[Generated _ViewBinding id and BindView implmentation desync](https://github.com/JakeWharton/butterknife/issues/897) 

![](imgs/20170815-153608.png)

  评论里也已经有人给出了解决方法，ButterKnife升级到8.6.0及以后的版本就可以了。
  
  
  ![](imgs/20170815-154037.png)


  开始还以为是代码的问题，现在看来是ButterKnife的锅，这个坑就这样填了，看来框架的迭代更新还是要及时追踪下，免得自己去做一些无用功。
    
  