
## Before You Begin

In the _config.yml file, the base URL is set to /blog is this themes gh-pages preview. It's recommended that you remove the base URL before working with this theme locally!

A Grunt environment is also included. There are a number of tasks it performs like minification of the JavaScript, compiling of the LESS files, adding banners to apply the MIT license, and watching for changes. Run the grunt default task by entering grunt into your command line which will build the files. You can use grunt watch if you are working on the JavaScript or the LESS.

You can run jekyll serve --watch and grunt watch at the same time to watch for changes and then build them all at once.

## Support

Visit blog overview page at https://github.com/machaojei/blog/ and leave a comment, email bazhuayu@2dfire.com, or open an issue here on GitHub for support.


